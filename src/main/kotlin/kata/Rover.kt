package kata

class Rover(var positionX: Int = 0, var positionY: Int = 0, var orientation: String = "N") {

    fun receivedCommands(command: List<String>) {
        command.forEach {

            if(it == "f"){
                when(this.orientation){
                    "N"->this.positionY = this.positionY + 1
                    "W"-> this.positionX = this.positionX - 1
                }
            }
            if(it == "b"){
                this.positionY = this.positionY - 1
            }
            if(it == "l"){
                when  (this.orientation) {
                    "N"->this.orientation = "W"
                    "W"->this.orientation = "S"
                }
            }
            if(it == "r"){
                when  (this.orientation) {
                    "N"->this.orientation = "E"
                    "E"->this.orientation = "S"
                    "S"->this.orientation = "W"
                    "W"->this.orientation = "N"

                }
            }
        }
    }

}