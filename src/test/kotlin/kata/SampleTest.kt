package kata

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class SampleTest {
    @Test
    fun `rover has a position and orientation`() {
        //given_rover
        val rover = Rover(0, 0, "N")
        //when_start_point_and_direction_not_given
        //then
        Assertions.assertEquals(0, rover.positionX)
        Assertions.assertEquals(0, rover.positionY)
        Assertions.assertEquals("N", rover.orientation)
    }

    @Test
    fun `rover facing north move forward one step`() {
        //given_rover
        val rover = Rover(0, 0, "N")
        //when_received_a_command
        rover.receivedCommands(listOf("f"))
        //then
        Assertions.assertEquals( 0, rover.positionX)
        Assertions.assertEquals( 1, rover.positionY)
        Assertions.assertEquals("N", rover.orientation)
    }

    @Test
    fun `rover move forward x2`() {
        //given_rover
        val rover = Rover(0, 0, "N")
        //when_received_a_command
        rover.receivedCommands(listOf("f","f"))
        //then
        Assertions.assertEquals( 0, rover.positionX)
        Assertions.assertEquals( 2, rover.positionY)
        Assertions.assertEquals("N", rover.orientation)
    }

    @Test
    fun `rover move backward`() {
        //given_rover
        val rover = Rover(0, 0, "N")
        //when_received_a_command
        rover.receivedCommands(listOf("b"))
        //then
        Assertions.assertEquals( 0, rover.positionX)
        Assertions.assertEquals( -1, rover.positionY)
        Assertions.assertEquals("N", rover.orientation)
    }

    //dado un rover mirando hacia el oeste(west)
    //cuando avanza una posicion
    // el eje X deciende 1
    @Test
    fun `rover facing west moves forward one step`(){
        val rover = Rover(0,0,"W")
        rover.receivedCommands(listOf("f"))
        Assertions.assertEquals(-1,rover.positionX)
        Assertions.assertEquals(0, rover.positionY)
        Assertions.assertEquals("W",rover.orientation)
    }

    @Test
    fun `rover turn left`() {
        //given_rover
        val rover = Rover(0, 0, "N")
        //when_received_a_command
        rover.receivedCommands(listOf("l"))
        //then
        Assertions.assertEquals( 0, rover.positionX)
        Assertions.assertEquals( 0, rover.positionY)
        Assertions.assertEquals("W", rover.orientation)
    }

    @Test
    fun `rover turn leftx2`() {
        //given_rover
        val rover = Rover(0, 0, "N")
        //when_received_a_command
        rover.receivedCommands(listOf("l","l"))
        //then
        Assertions.assertEquals( 0, rover.positionX)
        Assertions.assertEquals( 0, rover.positionY)
        Assertions.assertEquals("S", rover.orientation)
    }


    @Test
    fun `rover turn rightx2`() {
        //given_rover
        val rover = Rover(0, 0, "N")
        //when_received_a_command
        rover.receivedCommands(listOf("r","r"))
        //then
        Assertions.assertEquals( 0, rover.positionX)
        Assertions.assertEquals( 0, rover.positionY)
        Assertions.assertEquals("S", rover.orientation)
    }
    @Test
    fun `rover facing north turns right to east`() {
        //given_rover
        val rover = Rover(0, 0, "N")
        //when_received_a_command
        rover.receivedCommands(listOf("r"))
        //then
        Assertions.assertEquals( 0, rover.positionX)
        Assertions.assertEquals( 0, rover.positionY)
        Assertions.assertEquals("E", rover.orientation)
    }

    @Test
    fun `rover facing east turns right to south`() {
        //given_rover
        val rover = Rover(0, 0, "E")
        //when_received_a_command
        rover.receivedCommands(listOf("r"))
        //then
        Assertions.assertEquals( 0, rover.positionX)
        Assertions.assertEquals( 0, rover.positionY)
        Assertions.assertEquals("S", rover.orientation)
    }

    @Test
    fun `rover facing south turning right to west`(){
        //given_rover
        val rover = Rover(0,0,"S")

        rover.receivedCommands(listOf("r"))

        Assertions.assertEquals(0, rover.positionX)
        Assertions.assertEquals(0, rover.positionY)
        Assertions.assertEquals("W", rover.orientation)
    }
    @Test
    fun `rover facing west turning right to north`(){
        //given_rover
        val rover = Rover(0,0,"W")

        rover.receivedCommands(listOf("r"))

        Assertions.assertEquals(0, rover.positionX)
        Assertions.assertEquals(0, rover.positionY)
        Assertions.assertEquals("N", rover.orientation)
    }





}
